// GENERATED AUTOMATICALLY FROM 'Assets/Config/InputActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace Unavia.Helicopter
{
    public class @InputActions : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @InputActions()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputActions"",
    ""maps"": [
        {
            ""name"": ""Heli"",
            ""id"": ""f174cc11-f777-491a-b720-efc9330e3931"",
            ""actions"": [
                {
                    ""name"": ""Throttle"",
                    ""type"": ""Value"",
                    ""id"": ""14c11d73-3164-4691-b51a-b67a253cf6d4"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Pedal"",
                    ""type"": ""Value"",
                    ""id"": ""975d40bc-afa3-4941-a9f1-bb4406c92f37"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Collective"",
                    ""type"": ""Value"",
                    ""id"": ""8ddea7e2-7acf-4bc3-8324-82cf7eea3c2c"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Cyclic"",
                    ""type"": ""Value"",
                    ""id"": ""97337e0c-d2a9-4fd8-a45f-7c4772176266"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Keyboard 1D Axis"",
                    ""id"": ""edb79021-6ac6-4c2e-bd42-71502e5fcbff"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Throttle"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""85a4881c-2664-4dc2-b949-af9ff7e30651"",
                    ""path"": ""<Keyboard>/minus"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Throttle"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""f6eeaa2b-7ffd-4848-966b-d794055bb9a0"",
                    ""path"": ""<Keyboard>/equals"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Throttle"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Controller 1D Axis"",
                    ""id"": ""c2bc649a-f621-424c-9ff5-1570db17449a"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Throttle"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""0e4fb5e0-f383-4789-a81a-414a118c1378"",
                    ""path"": ""<Gamepad>/leftShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Throttle"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""5eb30716-3e19-4237-b42a-41a7c3ce0c9a"",
                    ""path"": ""<Gamepad>/rightShoulder"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Throttle"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Keyboard 1D Axis"",
                    ""id"": ""c4259564-b2f9-4f9c-b723-c0947413bfa3"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Pedal"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""76a1baa9-4493-4077-a8c2-cfc0b048c591"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Pedal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""48a05bdc-2e75-4e1d-93b5-6130445e03fc"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Pedal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Controller 1D Axis"",
                    ""id"": ""bf2541dd-24b8-441b-8558-412e9439a7cc"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Pedal"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""5d09329e-383a-4a81-b25f-d166a587909b"",
                    ""path"": ""<Gamepad>/rightStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Pedal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""2dfc0fbe-54d3-4d69-9e15-cfc7aa1812d1"",
                    ""path"": ""<Gamepad>/rightStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Pedal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Keyboard 2D Vector"",
                    ""id"": ""19ae7084-13af-40da-bff6-79304ca9b564"",
                    ""path"": ""2DVector(mode=1)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cyclic"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""7c214d8b-af76-4662-9f2e-1ec6555c964c"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Cyclic"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""a6fd87d5-3854-45c1-856a-fb938250d7f2"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Cyclic"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""272bb365-1b60-4011-8b5f-35cce3aec086"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Cyclic"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""199437d5-23ea-4d69-946b-8b5638437457"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Cyclic"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Controller 2D Vector"",
                    ""id"": ""830b31d0-3d9a-4b2e-9457-d7922ef8fa94"",
                    ""path"": ""2DVector(mode=2)"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Cyclic"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""Up"",
                    ""id"": ""f9770c4e-1538-4a75-bae3-4c5ede0b1984"",
                    ""path"": ""<Gamepad>/leftStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Cyclic"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Down"",
                    ""id"": ""6d1f7442-7f86-4434-8cc3-22b90ac03780"",
                    ""path"": ""<Gamepad>/leftStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Cyclic"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Left"",
                    ""id"": ""eae690a1-4f65-4818-98a6-7a06f339eb7d"",
                    ""path"": ""<Gamepad>/leftStick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Cyclic"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Right"",
                    ""id"": ""91ad6daa-9113-4ccf-800d-1b8401d95047"",
                    ""path"": ""<Gamepad>/leftStick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Cyclic"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Keyboard 1D Axis"",
                    ""id"": ""528134e3-d72e-413e-9cd9-83503da2e36b"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Collective"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""e30c838c-51d1-4bdd-bc8a-9ffdd513b323"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Collective"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""1d1b0bb4-cad6-440d-b7f5-ad9f70cc80b5"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Collective"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Controller 1D Axis"",
                    ""id"": ""3b28d5fe-dd5c-421d-bed4-afe4aa7413f3"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Collective"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""3e1192c5-2abc-44eb-9390-0e5f11bf24bd"",
                    ""path"": ""<Gamepad>/rightStick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Collective"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""9f4e3854-ee5d-4a38-ad2f-ad0053d83360"",
                    ""path"": ""<Gamepad>/rightStick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Controller"",
                    ""action"": ""Collective"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard"",
            ""bindingGroup"": ""Keyboard"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Controller"",
            ""bindingGroup"": ""Controller"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
            // Heli
            m_Heli = asset.FindActionMap("Heli", throwIfNotFound: true);
            m_Heli_Throttle = m_Heli.FindAction("Throttle", throwIfNotFound: true);
            m_Heli_Pedal = m_Heli.FindAction("Pedal", throwIfNotFound: true);
            m_Heli_Collective = m_Heli.FindAction("Collective", throwIfNotFound: true);
            m_Heli_Cyclic = m_Heli.FindAction("Cyclic", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Heli
        private readonly InputActionMap m_Heli;
        private IHeliActions m_HeliActionsCallbackInterface;
        private readonly InputAction m_Heli_Throttle;
        private readonly InputAction m_Heli_Pedal;
        private readonly InputAction m_Heli_Collective;
        private readonly InputAction m_Heli_Cyclic;
        public struct HeliActions
        {
            private @InputActions m_Wrapper;
            public HeliActions(@InputActions wrapper) { m_Wrapper = wrapper; }
            public InputAction @Throttle => m_Wrapper.m_Heli_Throttle;
            public InputAction @Pedal => m_Wrapper.m_Heli_Pedal;
            public InputAction @Collective => m_Wrapper.m_Heli_Collective;
            public InputAction @Cyclic => m_Wrapper.m_Heli_Cyclic;
            public InputActionMap Get() { return m_Wrapper.m_Heli; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(HeliActions set) { return set.Get(); }
            public void SetCallbacks(IHeliActions instance)
            {
                if (m_Wrapper.m_HeliActionsCallbackInterface != null)
                {
                    @Throttle.started -= m_Wrapper.m_HeliActionsCallbackInterface.OnThrottle;
                    @Throttle.performed -= m_Wrapper.m_HeliActionsCallbackInterface.OnThrottle;
                    @Throttle.canceled -= m_Wrapper.m_HeliActionsCallbackInterface.OnThrottle;
                    @Pedal.started -= m_Wrapper.m_HeliActionsCallbackInterface.OnPedal;
                    @Pedal.performed -= m_Wrapper.m_HeliActionsCallbackInterface.OnPedal;
                    @Pedal.canceled -= m_Wrapper.m_HeliActionsCallbackInterface.OnPedal;
                    @Collective.started -= m_Wrapper.m_HeliActionsCallbackInterface.OnCollective;
                    @Collective.performed -= m_Wrapper.m_HeliActionsCallbackInterface.OnCollective;
                    @Collective.canceled -= m_Wrapper.m_HeliActionsCallbackInterface.OnCollective;
                    @Cyclic.started -= m_Wrapper.m_HeliActionsCallbackInterface.OnCyclic;
                    @Cyclic.performed -= m_Wrapper.m_HeliActionsCallbackInterface.OnCyclic;
                    @Cyclic.canceled -= m_Wrapper.m_HeliActionsCallbackInterface.OnCyclic;
                }
                m_Wrapper.m_HeliActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Throttle.started += instance.OnThrottle;
                    @Throttle.performed += instance.OnThrottle;
                    @Throttle.canceled += instance.OnThrottle;
                    @Pedal.started += instance.OnPedal;
                    @Pedal.performed += instance.OnPedal;
                    @Pedal.canceled += instance.OnPedal;
                    @Collective.started += instance.OnCollective;
                    @Collective.performed += instance.OnCollective;
                    @Collective.canceled += instance.OnCollective;
                    @Cyclic.started += instance.OnCyclic;
                    @Cyclic.performed += instance.OnCyclic;
                    @Cyclic.canceled += instance.OnCyclic;
                }
            }
        }
        public HeliActions @Heli => new HeliActions(this);
        private int m_KeyboardSchemeIndex = -1;
        public InputControlScheme KeyboardScheme
        {
            get
            {
                if (m_KeyboardSchemeIndex == -1) m_KeyboardSchemeIndex = asset.FindControlSchemeIndex("Keyboard");
                return asset.controlSchemes[m_KeyboardSchemeIndex];
            }
        }
        private int m_ControllerSchemeIndex = -1;
        public InputControlScheme ControllerScheme
        {
            get
            {
                if (m_ControllerSchemeIndex == -1) m_ControllerSchemeIndex = asset.FindControlSchemeIndex("Controller");
                return asset.controlSchemes[m_ControllerSchemeIndex];
            }
        }
        public interface IHeliActions
        {
            void OnThrottle(InputAction.CallbackContext context);
            void OnPedal(InputAction.CallbackContext context);
            void OnCollective(InputAction.CallbackContext context);
            void OnCyclic(InputAction.CallbackContext context);
        }
    }
}

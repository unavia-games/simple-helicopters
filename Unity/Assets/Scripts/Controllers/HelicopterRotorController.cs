using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Unavia.Helicopter
{
    public class HelicopterRotorController : MonoBehaviour
    {
        #region Properties
        [SerializeField]
        private float maxDegreesPerSecond = 3000f;
        #endregion

        private List<IHelicopterRotor> rotors;


        #region Unity Methods
        private void Start()
        {
            rotors = GetComponentsInChildren<IHelicopterRotor>().ToList();
        }
        #endregion


        #region Custom Methods
        /// <summary>
        /// Update all helicopter rotors
        /// </summary>
        /// <param name="input">Helicopter input</param>
        /// <param name="rpm">Helicopter engine RPM</param>
        public void UpdateRotors(HelicopterInput input, float rpm)
        {
            if (rotors.Count <= 0) return;

            float degreesPerSecond = (rpm * 360f) / 60f;
            degreesPerSecond = Mathf.Clamp(degreesPerSecond, 0f, maxDegreesPerSecond);

            // Debug.Log($"Throttle: {input.StickyThrottle:F2}  -  RPM: {rpm:F2}  -  DPS: {degreesPerSecond}");

            rotors.ForEach(rotor =>
            {
                rotor.UpdateRotor(input, degreesPerSecond);
            });
        }
        #endregion
    }
}

using System.Collections.Generic;
using UnityEngine;

namespace Unavia.Helicopter
{
    [RequireComponent(typeof(HelicopterInput), typeof(HelicopterCharacteristics))]
    public class HelicopterController : RigidBodyController
    {
        #region Properties
        [Header("Helicopter Properties")]
        public List<HelicopterEngine> Engines = new List<HelicopterEngine>();
        public HelicopterRotorController RotorController;
        #endregion

        private HelicopterInput input;
        private HelicopterCharacteristics characteristics;


        #region Unity Methods
        public override void Start()
        {
            base.Start();

            input = GetComponent<HelicopterInput>();
            characteristics = GetComponent<HelicopterCharacteristics>();
        }
        #endregion


        #region Custom Methods
        /// <summary>
        /// Handle helicopter physics
        /// </summary>
        protected override void HandlePhysics()
        {
            HandleEngines();
            HandleRotors();
            HandleCharacteristics();
        }

        /// <summary>
        /// Handle helicopter engines
        /// </summary>
        protected virtual void HandleEngines()
        {
            Engines.ForEach(engine =>
            {
                engine.UpdateEngine(input.StickyThrottle);
            });
        }

        /// <summary>
        /// Handle helicopter rotors
        /// </summary>
        protected virtual void HandleRotors()
        {
            if (!RotorController || Engines.Count <= 0) return;

            RotorController.UpdateRotors(input, Engines[0].RPM);
        }

        /// <summary>
        /// Handle helicopter flight characteristics
        /// </summary>
        protected virtual void HandleCharacteristics()
        {
            if (!characteristics) return;

            characteristics.UpdateCharacteristics();
        }
        #endregion
    }
}

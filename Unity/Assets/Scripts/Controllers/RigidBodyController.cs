using UnityEngine;

namespace Unavia.Helicopter
{
    /// <summary>
    /// Generic rigidbody controller with physics support
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class RigidBodyController : MonoBehaviour
    {
        #region Properties
        [Header("Base Properties")]
        /// <summary>
        /// Rigidbody mass (kg)
        /// </summary>
        public float Mass = 100f;
        /// <summary>
        /// Center of gravity Transform for rigidbody
        /// </summary>
        public Transform CenterOfGravity;
        #endregion

        /// <summary>
        /// Rigidbody component
        /// </summary>
        protected Rigidbody rb;


        #region Unity Methods
        /// <summary>
        /// Rigidbody start method
        /// </summary>
        public virtual void Start()
        {
            rb = GetComponent<Rigidbody>();

            rb.centerOfMass = CenterOfGravity.position;

            SetMass(Mass);
        }

        void FixedUpdate()
        {
            if (!rb) return;

            rb.mass = Mass;

            HandlePhysics();
        }
        #endregion


        #region Custom Methods
        /// <summary>
        /// Handle rigidbody physics
        /// </summary>
        protected virtual void HandlePhysics() { }

        /// <summary>
        /// Set the rigidbody mass
        /// </summary>
        /// <param name="mass">Rigidbody mass</param>
        protected void SetMass(float mass)
        {
            Mass = mass;
            rb.mass = mass;
        }
        #endregion
    }
}

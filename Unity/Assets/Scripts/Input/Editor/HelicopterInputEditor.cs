using UnityEditor;

namespace Unavia.Helicopter
{
    [CustomEditor(typeof(HelicopterInput))]
    public class HeliInputEditor : Editor
    {
        #region Properties
        #endregion

        private HelicopterInput targetInput;


        #region Unity Methods
        private void OnEnable()
        {
            targetInput = (HelicopterInput)target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            DrawDebugUI();

            Repaint();
        }
        #endregion


        #region Custom Methods
        /// <summary>
        /// Draw a debug UI with input values
        /// </summary>
        private void DrawDebugUI()
        {
            EditorGUILayout.Space();
            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUILayout.Space();

            EditorGUI.indentLevel++;
            EditorGUILayout.LabelField($"Raw Throttle: {targetInput.RawThrottle:F2}");
            EditorGUILayout.LabelField($"Sticky Throttle: {targetInput.StickyThrottle:F2}");
            EditorGUILayout.LabelField($"Collective: {targetInput.RawCollective:F2}");
            EditorGUILayout.LabelField($"Sticky Collective: {targetInput.StickyCollective:F2}");
            EditorGUILayout.LabelField($"Cyclic: {targetInput.Cyclic:F2}");
            EditorGUILayout.LabelField($"Pedal: {targetInput.Pedal:F2}");
            EditorGUI.indentLevel--;

            EditorGUILayout.Space();
            EditorGUILayout.EndVertical();
        }
        #endregion
    }
}

using System;
using UnityEngine;

namespace Unavia.Helicopter
{
    public class HelicopterInput : MonoBehaviour
    {
        #region Properties
        /// <summary>
        /// Deadzone for stick input
        /// </summary>
        [SerializeField]
        [Range(0f, 0.25f)]
        private float stickDeadzone = 0.15f;

        public float RawThrottle { get; private set; } = 0f;
        // TODO: Introduce multiplier to slow delta speed
        public float StickyThrottle { get; private set; } = 0f;
        public float RawCollective { get; private set; } = 0f;
        // TODO: Introduce multiplier to slow delta speed
        public float StickyCollective { get; private set; } = 0f;
        public Vector2 Cyclic { get; private set; } = Vector2.zero;
        public float Pedal { get; private set; } = 0f;
        #endregion

        /// <summary>
        /// Input action map
        /// </summary>
        private InputActions inputActions;

        private float deadzone = 0.2f;


        #region Unity Methods
        void Awake()
        {
            inputActions = new InputActions();
        }

        void Update()
        {
            HandleInput();
            HandleStickyThrottle();
            HandleStickyCollective();
        }

        private void OnEnable()
        {
            inputActions.Enable();
        }

        private void OnDisable()
        {
            inputActions.Disable();
        }
        #endregion


        #region Custom Methods
        /// <summary>
        /// Deadzone 
        /// <remarks>
        /// Kinda a hack but necessary because input deadzones incorrectly clamp a 2d-axis
        /// at a split value if both x and y axis are 1 (will clamp at 0.71)!
        /// Also, since buttons are on/off, this does not affect keyboard input!
        /// </remarks>
        /// </summary>
        /// <param name="value">Input value</param>
        /// <param name="deadzone">Deadzone percentage</param>
        /// <returns>Deadzoned</returns>
        private float DeadzoneFloat(float value, float deadzone)
        {
            return Mathf.Abs(value) < deadzone ? 0f : value.Map(deadzone, 1f, 0f, 1f);
        }

        /// <summary>
        /// Smooth float input from InputAction (gives rounded digital value)
        /// </summary>
        /// <remarks>
        /// Summarized from: https://forum.unity.com/threads/axis-gravity-smoothing.943131/#post-6158024
        /// </summary>
        /// <param name="value">Current value</param>
        /// <param name="input">Input value</param>
        /// <param name="sensitivity">Change sensitivity</param>
        /// <returns>Smoothed float input</returns>
        private float SmoothInputFloat(float value, float input, float sensitivity = 4f)
        {
            float deadzonedInput = DeadzoneFloat(input, stickDeadzone);

            // Different times for sensitivty (acceleration towards) and gravity (deceleration away)
            //   requires an additional check for whether input is 0 or moving.
            float final = Mathf.MoveTowards(value, deadzonedInput, sensitivity * Time.deltaTime);
            return Mathf.Clamp(final, -1f, 1f);
        }

        /// <summary>
        /// Smoother vector input from InputAction (gives rounded digital values)
        /// </summary>
        /// <param name="value">Current value</param>
        /// <param name="input">Input value</param>
        /// <param name="sensitivity">Change sensitivity</param>
        /// <returns>Smoother vector input</returns>
        private Vector2 SmoothInputVector2(Vector2 value, Vector2 input, float sensitivity = 4f)
        {
            return new Vector2(
                SmoothInputFloat(value.x, input.x, sensitivity),
                SmoothInputFloat(value.y, input.y, sensitivity)
            );
        }

        /// <summary>
        /// Handle input
        /// </summary>
        private void HandleInput()
        {
            Vector2 cyclicInput = inputActions.Heli.Cyclic.ReadValue<Vector2>();
            Cyclic = SmoothInputVector2(Cyclic, cyclicInput);

            float collectiveInput = inputActions.Heli.Collective.ReadValue<float>();
            RawCollective = SmoothInputFloat(RawCollective, collectiveInput);

            float throttleInput = inputActions.Heli.Throttle.ReadValue<float>();
            RawThrottle = SmoothInputFloat(RawThrottle, throttleInput);

            float pedalInput = inputActions.Heli.Pedal.ReadValue<float>();
            Pedal = SmoothInputFloat(Pedal, pedalInput);
        }

        /// <summary>
        /// Handle sticky throttle
        /// </summary>
        private void HandleStickyThrottle()
        {
            StickyThrottle += RawThrottle * Time.deltaTime;
            StickyThrottle = Mathf.Clamp01(StickyThrottle);
        }

        /// <summary>
        /// Handle sticky collective
        /// </summary>
        private void HandleStickyCollective()
        {
            StickyCollective += RawCollective * Time.deltaTime;
            StickyCollective = Mathf.Clamp01(StickyCollective);
        }
        #endregion
    }
}

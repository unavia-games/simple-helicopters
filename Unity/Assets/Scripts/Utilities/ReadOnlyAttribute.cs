using UnityEngine;

namespace Unavia.Helicopter
{
    /// <summary>
    /// Mark a property as read-only
    /// </summary>
    /// <remarks>Taken from: https://www.patrykgalach.com/2020/01/20/readonly-attribute-in-unity-editor </remarks>
    public class ReadOnlyAttribute : PropertyAttribute { }
}

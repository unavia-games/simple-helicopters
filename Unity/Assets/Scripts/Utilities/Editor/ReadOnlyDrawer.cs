using UnityEngine;
using UnityEditor;

namespace Unavia.Helicopter
{
    /// <summary>
    /// Drawer for read-only attribute
    /// </summary>
    /// <remarks>Taken from: https://www.patrykgalach.com/2020/01/20/readonly-attribute-in-unity-editor </remarks>
    [CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
    public class ReadOnlyDrawer : PropertyDrawer
    {
        /// <summary>
        /// Unity method for drawing GUI in Editor
        /// </summary>
        /// <param name="position">Drawer position</param>
        /// <param name="property">Drawer property</param>
        /// <param name="label">Drawer label</param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var previousGUIState = GUI.enabled;

            GUI.enabled = false;

            EditorGUI.PropertyField(position, property, label);

            GUI.enabled = previousGUIState;
        }
    }

}

﻿using UnityEngine;

namespace Unavia.Helicopter
{
    public static class FloatUtilities
    {
        /// <summary>
        /// Map a value from one range to another
        /// </summary>
        /// <param name="value">Input value</param>
        /// <param name="fromMin">Minimum input range value</param>
        /// <param name="fromMax">Maximum input range value</param>
        /// <param name="toMin">Minimum output range value</param>
        /// <param name="toMax">Maximum output range value</param>
        /// <returns>Mapped value</returns>
        public static float Map(this float value, float fromMin, float fromMax, float toMin, float toMax)
        {
            return (value - fromMin) / (fromMax - fromMin) * (toMax - toMin) + toMin;
        }

        /// <summary>
        /// Map and clamp a value from one range to another
        /// </summary>
        /// <param name="value">Input value</param>
        /// <param name="fromMin">Minimum input range value</param>
        /// <param name="fromMax">Maximum input range value</param>
        /// <param name="toMin">Minimum output range value</param>
        /// <param name="toMax">Maximum output range value</param>
        /// <returns>Clamped mapped value</returns>
        public static float MapClamped(this float value, float fromMin, float fromMax, float toMin, float toMax)
        {
            float unclampedValue = value.Map(fromMin, fromMax, toMin, toMax);
            return Mathf.Clamp(unclampedValue, toMin, toMax);
        }

        /// <summary>
        /// Map a value between 0 and 1 to another range
        /// </summary>
        /// <param name="value">Input value</param>
        /// <param name="toMin">Minimum output range value</param>
        /// <param name="toMax">Maximum output range value</param>
        /// <returns>Mapped value</returns>
        public static float MapFrom01(this float value, float toMin, float toMax)
        {
            float clampedValue = Mathf.Clamp01(value);
            return clampedValue.Map(0, 1, toMin, toMax);
        }

        /// <summary>
        /// Map a value from a range to between 0 and 1
        /// </summary>
        /// <param name="value">Input value</param>
        /// <param name="fromMin">Minimum input range value</param>
        /// <param name="fromMax">Maximum input range value</param>
        /// <returns>Mapped value</returns>
        public static float MapTo01(this float value, float fromMin, float fromMax)
        {
            float unclampedValue = value.Map(fromMin, fromMax, 0f, 1f);
            return Mathf.Clamp(unclampedValue, 0f, 1f);
        }
    }
}

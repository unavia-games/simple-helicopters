using UnityEngine;

namespace Unavia.Helicopter
{
    public class HelicopterEngine : MonoBehaviour
    {
        #region Properties
        public float MaxHorsePower = 140f;
        public float MaxRPM = 2700f;
        public float PowerDelay = 0.2f;
        public AnimationCurve PowerCurve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

        [SerializeField]
        [ReadOnly]
        private float horsePower;
        [SerializeField]
        [ReadOnly]
        private float rpm;

        public float HorsePower { get; private set; }
        public float RPM { get; private set; }
        #endregion


        #region Unity Methods
        #endregion


        #region Custom Methods
        /// <summary>
        /// Handle engine power/rpm from throttle input
        /// </summary>
        /// <param name="throttle">Normalized throttle value</param>
        public void UpdateEngine(float throttle)
        {
            float targetHorsePower = PowerCurve.Evaluate(throttle) * MaxHorsePower;
            HorsePower = Mathf.Lerp(HorsePower, targetHorsePower, Time.deltaTime * PowerDelay);

            float targetRPM = throttle * MaxRPM;
            RPM = Mathf.Lerp(RPM, targetRPM, Time.deltaTime * PowerDelay);

            // DEBUG: Show values in inspector
            horsePower = HorsePower;
            rpm = RPM;

            // NOTE: Attempt at a better use of smoothing towards a target over time
            //float velocity = 0f;
            //RPM = Mathf.SmoothDamp(RPM, targetRPM, ref velocity, PowerDelay);
        }
        #endregion
    }
}

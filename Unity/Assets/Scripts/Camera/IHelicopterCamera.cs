﻿namespace Unavia.Helicopter
{
    public interface IHelicopterCamera
    {
        void UpdateCamera(bool smoothMovement);
    }
}

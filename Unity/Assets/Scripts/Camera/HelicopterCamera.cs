using UnityEngine;

namespace Unavia.Helicopter
{
    public class HelicopterCamera : MonoBehaviour, IHelicopterCamera
    {
        #region Properties
        [SerializeField]
        private GameObject target;
        [SerializeField]
        private Transform lookPoint;

        [Header("Camera Properties")]
        /// <summary>
        /// Follow height above target
        /// </summary>
        [SerializeField]
        private float height = 3f;
        /// <summary>
        /// Follow distance behind target
        /// </summary>
        [SerializeField]
        private float distance = 6f;
        /// <summary>
        /// Camera movement smooth speed
        /// </summary>
        [SerializeField]
        private float smoothSpeed = 0.35f;
        #endregion

        private Rigidbody targetRb;
        private Vector3 wantedPosition;
        private Vector3 cameraVelocity;


        #region Unity Methods
        void Start()
        {
            if (!target) return;

            targetRb = target.GetComponent<Rigidbody>();

            // Immediately orient camera (skipping smooth time)
            UpdateCamera(true);
        }

        void FixedUpdate()
        {
            if (!targetRb) return;

            UpdateCamera();
        }
        #endregion


        #region Custom Methods
        public void UpdateCamera(bool smoothMovement = false)
        {
            Vector3 flatForward = -target.transform.forward;
            flatForward.y = 0;
            flatForward = flatForward.normalized;

            // Place camera behind the target at a suitable distance
            wantedPosition = target.transform.position + (flatForward * distance) + (Vector3.up * height);
            transform.position = smoothMovement
                ? wantedPosition
                : Vector3.SmoothDamp(transform.position, wantedPosition, ref cameraVelocity, smoothSpeed);

            transform.LookAt(lookPoint ? lookPoint : target.transform);
        }
        #endregion
    }
}

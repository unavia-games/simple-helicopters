using System.ComponentModel;
using UnityEngine;

namespace Unavia.Helicopter
{
    /// <summary>
    /// Helicopter flight characterstics
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(HelicopterInput))]
    public class HelicopterCharacteristics : MonoBehaviour
    {
        #region Properties
        [Header("Lift properties")]
        public float MaxLiftForce = 10f;
        public HelicopterMainRotor mainRotor;

        [Header("Cyclic properties")]
        // Cyclic force controls forces applied to rotate helicopter
        public float CyclicForce = 2f;
        // Cyclic force multiplier controls forces (speed) applied to helicopter based on angles
        public float CyclicForceMultiplier = 4f;

        [Header("Tail rotor properties")]
        public float TailForce = 2f;

        [Header("Auto level properties")]
        public float AutoLevelForce = 2f;
        #endregion

        private Rigidbody rb;
        private HelicopterInput input;
        private float cycliceForceBaseMultiplier = 1000f;

        private Vector3 flatForward;
        private float forwardDot;
        private Vector3 flatRight;
        private float rightDot;


        #region Unity Methods
        private void Start()
        {
            rb = GetComponent<Rigidbody>();
            input = GetComponent<HelicopterInput>();
        }
        #endregion


        #region Custom Methods
        /// <summary>
        /// Update the helicopter flight characteristics
        /// </summary>
        public void UpdateCharacteristics()
        {
            if (!rb || !input) return;

            HandleLift();
            HandleCyclic();
            HandlePedals();

            CalculateAngles();
            AutoLevel();
        }

        /// <summary>
        /// Handle helicopter lift
        /// </summary>
        protected virtual void HandleLift()
        {
            if (!mainRotor) return;

            Vector3 liftForce = transform.up * (Physics.gravity.magnitude + MaxLiftForce) * rb.mass;

            // TODO: Refactor
            float normalizedRpms = mainRotor.RPM / 500f;

            rb.AddForce(liftForce * Mathf.Pow(normalizedRpms, 2f) * Mathf.Pow(input.StickyCollective + 0.5f, 2f), ForceMode.Force);
        }

        /// <summary>
        /// Handle helicopter cyclic input
        /// </summary>
        protected virtual void HandleCyclic()
        {
            float cyclicZForce = input.Cyclic.x * CyclicForce;
            float cyclicXForce = input.Cyclic.y * CyclicForce;

            // Cyclic use local coordinate system
            rb.AddRelativeTorque(Vector3.forward * -cyclicZForce, ForceMode.Acceleration);
            rb.AddRelativeTorque(Vector3.right * cyclicXForce, ForceMode.Acceleration);

            // Apply additional force based off of helicopter angles
            Vector3 forwardVector = flatForward * forwardDot;
            Vector3 rightVector = flatRight * rightDot;
            Vector3 cyclicDirection = Vector3.ClampMagnitude(forwardVector + rightVector, 1f);
            float cyclicMultiplier = CyclicForceMultiplier * cycliceForceBaseMultiplier;
            rb.AddForce(cyclicDirection * CyclicForce * cyclicMultiplier, ForceMode.Force);
        }
        
        /// <summary>
        /// Handle helicopter pedals
        /// </summary>
        protected virtual void HandlePedals()
        {
            // Pedals use global coordinate system
            rb.AddTorque(Vector3.up * input.Pedal * TailForce, ForceMode.Acceleration);
        }

        /// <summary>
        /// Calculate helicopter angles
        /// </summary>
        /// <remarks>Flat vectors enable further angle calculations</remarks>
        private void CalculateAngles()
        {
            flatForward = transform.forward;
            flatForward.y = 0f;
            flatForward = flatForward.normalized;

            flatRight = transform.right;
            flatRight.y = 0f;
            flatRight = flatRight.normalized;

            // Compare current directions against flat angle directions to find actual angles
            forwardDot = Vector3.Dot(transform.up, flatForward);
            rightDot = Vector3.Dot(transform.up, flatRight);
        }

        /// <summary>
        /// Auto-level the helicopter
        /// </summary>
        private void AutoLevel()
        {
            float forwardForce = rightDot * AutoLevelForce;
            float rightForce = -forwardDot * AutoLevelForce;

            rb.AddRelativeTorque(Vector3.forward * forwardForce, ForceMode.Acceleration);
            rb.AddRelativeTorque(Vector3.right * rightForce, ForceMode.Acceleration);
        }
        #endregion
    }
}

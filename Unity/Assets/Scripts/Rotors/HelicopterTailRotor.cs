using UnityEngine;

namespace Unavia.Helicopter
{
    public class HelicopterTailRotor : MonoBehaviour, IHelicopterRotor
    {
        #region Properties
        [SerializeField]
        private Transform rotorTransform;
        public Transform RotorTransform => rotorTransform;

        [SerializeField]
        private Transform leftRotor;
        [SerializeField]
        private Transform rightRotor;
        [SerializeField]
        private float maxPitch = 45f;
        [SerializeField]
        private float rotationSpeedModifier = 1.5f;
        #endregion


        #region Unity Methods
        #endregion


        #region Custom Methods
        public void UpdateRotor(HelicopterInput input, float degreesPerSecond)
        {
            rotorTransform.Rotate(Vector3.right, degreesPerSecond * rotationSpeedModifier * Time.deltaTime);

            // Pitch the blades according to pedal
            if (leftRotor && rightRotor)
            {
                leftRotor.localRotation = Quaternion.Euler(0f, input.Pedal * maxPitch, 0f);
                rightRotor.localRotation = Quaternion.Euler(0f, -input.Pedal * maxPitch, 0f);
            }
        }
        #endregion
    }
}

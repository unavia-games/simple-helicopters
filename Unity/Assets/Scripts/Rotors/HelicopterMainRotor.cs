using UnityEngine;

namespace Unavia.Helicopter
{
    public class HelicopterMainRotor : MonoBehaviour, IHelicopterRotor
    {
        #region Properties
        [SerializeField]
        private Transform rotorTransform;
        public Transform RotorTransform => rotorTransform;

        [SerializeField]
        private Transform leftRotor;
        [SerializeField]
        private Transform rightRotor;
        [SerializeField]
        private float maxPitch = 35f;
        [SerializeField]
        private float rotationSpeedModifier = 0.5f;

        [HideInInspector]
        public float RPM { get; private set; }
        #endregion


        #region Unity Methods
        #endregion


        #region Custom Methods
        public void UpdateRotor(HelicopterInput input, float degreesPerSecond)
        {
            RPM = (degreesPerSecond / 360f) * 60f;

            // rotorTransform.Rotate(Vector3.up, degreesPerSecond);

            // TODO: Figure out and extract
            rotorTransform.Rotate(Vector3.up, degreesPerSecond * rotationSpeedModifier * Time.deltaTime);

            // Pitch the blades according to collective
            if (leftRotor && rightRotor)
            {
                leftRotor.localRotation = Quaternion.Euler(-input.StickyCollective * maxPitch, 0f, 0f);
                rightRotor.localRotation = Quaternion.Euler(input.StickyCollective * maxPitch, 0f, 0f);
            }
        }
        #endregion
    }
}

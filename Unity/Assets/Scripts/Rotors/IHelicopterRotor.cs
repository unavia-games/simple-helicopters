using UnityEngine;

namespace Unavia.Helicopter
{
    public interface IHelicopterRotor
    {
        #region Properties
        Transform RotorTransform { get; }
        #endregion


        #region Custom Methods
        /// <summary>
        /// Update a helicopter rotor
        /// </summary>
        /// <param name="input">Helicopter input</param>
        /// <param name="degreesPerSecond">Rotation degrees per second</param>
        void UpdateRotor(HelicopterInput input, float degreesPerSecond);
        #endregion
    }
}

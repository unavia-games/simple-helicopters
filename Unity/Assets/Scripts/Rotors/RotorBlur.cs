using System;
using System.Collections.Generic;
using UnityEngine;

namespace Unavia.Helicopter
{
    public class RotorBlur : MonoBehaviour, IHelicopterRotor
    {
        #region Properties
        [SerializeField]
        private Transform rotorTransform;
        public Transform RotorTransform => rotorTransform;

        [Header("Rotor Blur Properties")]
        /// <summary>
        /// Value that indicates highest rotor blade LOD
        /// </summary>
        [SerializeField]
        private float maxDegreesPerSecond = 3000f;
        [SerializeField]
        private List<GameObject> blades = new List<GameObject>();
        [SerializeField]
        private GameObject blurQuad;

        [SerializeField]
        private List<Texture2D> blurTextures = new List<Texture2D>();
        #endregion

        private Material blurMaterial;


        #region Unity Methods
        private void Start()
        {
            blurMaterial = blurQuad.GetComponent<Renderer>().material;
        }
        #endregion


        #region Custom Methods
        public void UpdateRotor(HelicopterInput input, float degreesPerSecond)
        {
            float normalizedDegreesPerSecond = Mathf.InverseLerp(0f, maxDegreesPerSecond, degreesPerSecond);
            int blurTextureId = Mathf.FloorToInt(normalizedDegreesPerSecond * blurTextures.Count - 1);
            blurTextureId = Mathf.Clamp(blurTextureId, 0, blurTextures.Count - 1);

            blurMaterial.SetTexture("_BaseMap", blurTextures[blurTextureId]);

            ToggleRotors(blurTextureId <= 1);
        }

        // TODO: Hide rotor quad when physical blades are shown

        /// <summary>
        /// Toggle blade object visibility (based on blur texture)
        /// </summary>
        /// <param name="shown">Whether blade object is visible</param>
        private void ToggleRotors(bool shown)
        {
            if (blades.Count <= 0) return;

            blades.ForEach(blade => blade.SetActive(shown));
        }
        #endregion
    }
}

# Simple Helicopters

Simple package exploring helicopter flight (from Udemy course by IndiePixel).

### Prerequisites

- Unity 2020.2

### Packages

- **Input System** - Unity input system
- **Universal RP** - Universal render pipeline

